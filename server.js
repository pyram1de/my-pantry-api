//  OpenShift sample Node application
var express = require('express'),
    app     = express(),
    morgan  = require('morgan'),
    jwt = require('express-jwt'),
    jwks = require('jwks-rsa'),
    ObjectId = require('mongodb').ObjectID,
    bodyParser = require('body-parser'),
    MongoClient = require('mongodb').MongoClient;
    
Object.assign=require('object-assign')
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.engine('html', require('ejs').renderFile);
app.use(morgan('combined'))



app.use(function(req, res, next) {
  if (req.method === 'OPTIONS') {
    
    console.log('!OPTIONS');
    var headers = {};
    // IE8 does not allow domains to be specified, just the *
    console.log('origin', req.headers.origin);
    headers["Access-Control-Allow-Origin"] = req.headers.origin;
    //headers["Access-Control-Allow-Origin"] = "*";
    headers["Access-Control-Allow-Methods"] = "GET,PUT,POST,DELETE,PATCH,OPTIONS";
    headers["Access-Control-Allow-Credentials"] = true;
    headers["Access-Control-Max-Age"] = '86400'; // 24 hours
    headers["Access-Control-Allow-Headers"] = "X-Requested-With, X-HTTP-Method-Override, Content-Type, Accept, Authorization";
    res.writeHead(200, headers);
    res.end();
    return;
  } else {
    
  }
  res.header("Access-Control-Allow-Origin", req.headers.origin);
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization");
  res.header("Access-Control-Allow-Methods", "GET,PUT,POST,DELETE,PATCH,OPTIONS")
  res.header("Access-Control-Allow-Credentials", true)
  next();
});

var port = process.env.PORT || process.env.OPENSHIFT_NODEJS_PORT || 8080,
    ip   = process.env.IP   || process.env.OPENSHIFT_NODEJS_IP || '0.0.0.0',
    mongoURL = process.env.OPENSHIFT_MONGODB_DB_URL || process.env.MONGO_URL,
    mongoURLLabel = "";

if (mongoURL == null && process.env.DATABASE_SERVICE_NAME) {
  var mongoServiceName = process.env.DATABASE_SERVICE_NAME.toUpperCase(),
      mongoHost = process.env[mongoServiceName + '_SERVICE_HOST'],
      mongoPort = process.env[mongoServiceName + '_SERVICE_PORT'],
      mongoDatabase = process.env[mongoServiceName + '_DATABASE'],
      mongoPassword = process.env[mongoServiceName + '_PASSWORD']
      mongoUser = process.env[mongoServiceName + '_USER'];
  if (mongoHost && mongoPort && mongoDatabase) {
    mongoURL = mongoHost + "://" + mongoUser + ':' + mongoPassword + '@' + mongoPort + '/' +  mongoDatabase;
    mongoURLLabel = mongoHost + ":" + mongoPort + '/' + mongoDatabase;
  }
}
var db = null,
    dbDetails = new Object();

var initDb = function(callback) {
  if (mongoURL == null) return;

  var mongodb = require('mongodb');
  if (mongodb == null) return;
  console.log('MongoUrl', mongoURL);
  
  MongoClient.connect(mongoURL, function(err, client) {
    if (err) {
      callback(err);
      return;
    }

    db = client.db('pantry');
    dbDetails.databaseName = db.databaseName;
    console.log('DB=', db.databaseName);
    dbDetails.url = mongoURLLabel;
    dbDetails.type = 'MongoDB';

    console.log('Connected to MongoDB at: %s', mongoURL);
  });
};

app.get('/', function (req, res) {
  // try to initialize the db on every request if it's not already
  // initialized.
  if (!db) {
    initDb(function(err){});
  }
  if (db) {
    var col = db.collection('counts');
    // Create a document with request IP and current time of request
    col.insert({ip: req.ip, date: Date.now()});
    col.count(function(err, count){
      if (err) {
        console.log('Error running count. Message:\n'+err);
      }
      res.render('index.html', { pageCountMessage : count, dbInfo: dbDetails });
    });
  } else {
    res.render('index.html', { pageCountMessage : null});
  }
});

app.post('/api/household', function(req, res){
  // todo check that the user actually has access to the household
  let household = req.body;
  let householdId = household._id;
  delete household._id;
  console.log('the ID?', householdId);
  console.log('the household', household);

  db.collection('households').updateOne({
      _id: ObjectId(householdId)
      },
        household
      ).then(done=>{
        console.log('household saved');
        res.status(200).json(done.result)
  });
});

app.post('/api/user/setselectedhousehold/:householdId', function(req, res){
  let householdId = req.params.householdId;
  let user = req.body;
  console.log('setting selected household for', user.name);
  db.collection('users').updateOne({
    _id: ObjectId(user._id)
  }, {
    $set : {
      selectedHousehold: ObjectId(householdId)
    }
  }).then(done=>{
    console.log('SUCCESS');
    res.status(200).json(done.result);
  });
});

app.post('/api/user', function(req, res){
    let user = req.body;
    let userId = user._id;
    delete user._id;

    db.collection('users').updateOne({
            _id: ObjectId(userId)
        },
        user
    ).then(done=>{
      console.log('user saved!');
        res.status(200).json(done.result)
    });
});

app.get('/api/gethouseholds', function(req, res){
  var user_id = ObjectId(req.query.userId);
  // get the user from the db.
  db.collection("users").findOne({
    _id: ObjectId(user_id)
  }).then(
    (user)=>{
      // then get the list of household ids.
      if(user.households&&user.households.length>0){
        console.log('found user');
        db.collection("households").find(
          {_id : {$in : user.households}}
        ).toArray(function(err, docs){
          if(!err){
            console.log('found households');
            res.json(docs);
          }
        });
      } else {
        res.json([]);
      }
    }, 
    (err)=>{
      console.log('couldnt find user');
      res.status(404).send('cannot find user');
    });
});

app.post('/api/createHousehold', function(req, res){
  //TODO need to cater for users who already have a household
  console.log('in the post method for createhousehold', req.body);
  let houseHold = req.body.household;
  let user_id = req.body.user._id;
  
  // name?
  db.collection("households").insertOne(houseHold, (err, householdsResult)=>{
    if(!err){
      var household_id = householdsResult.insertedId;
      // update user to include this household.
      db.collection("users").findOne({
        _id: ObjectId(user_id)
      }).then((user)=>{
        // push the users household 
        var households = user.households || [];
        households.push(ObjectId(household_id));

        db.collection("users").updateOne({
          _id: ObjectId(user_id)
        }, {
          $set : {
            households : households
          }
        },
        {
          upsert: true
        }
        , (err, updatedUserResult)=> {
          if(!err){
            res.json(householdsResult);
          }
        });
      });
    } else {
      console.log('ERROR', err);
    }
  });
});


app.post('/api/userlogin', function(req, res){
  let user = req.body;
  db.collection("users").findOne({sub: { $eq: user.sub }}).then((existingUser)=>{
    if(existingUser){
        // user already exists
        res.json(existingUser);
    } else {
        db.collection("users").insertOne(user, (err, user)=>{
          if(!err){
          res.json(user);
          } else {
            console.log('ERROR when creating user', err);
          }
        });
    }
  }, (err)=>{
    console.log('im assuming this is an error', err);
  });
});

// error handling
app.use(function(err, req, res, next){
  console.error(err.stack);
  res.status(500).send('Something bad happened!');
});

initDb(function(err){
  console.log('Error connecting to Mongo. Message:\n'+err);
});

app.listen(port, ip);
console.log('Server running on http://%s:%s', ip, port);

var jwtCheck = jwt({
  secret: jwks.expressJwtSecret({
      cache: true,
      rateLimit: true,
      jwksRequestsPerMinute: 5,
      jwksUri: "https://pantry-app.eu.auth0.com/.well-known/jwks.json"
  }),
  audience: 'https://pantry-app-secure-api-my-pantry.a3c1.starter-us-west-1.openshiftapps.com/',
  issuer: "https://pantry-app.eu.auth0.com/",
  algorithms: ['RS256']
});

app.use(jwtCheck);

app.get('/authorized', function (req, res) {
  res.send('Secured Resource');
});

app.get('*', function(req, res){
  res.status(404).send(
    "not found"
  );
  console.log('404 for', req.originalUrl);
})

module.exports = app ;
